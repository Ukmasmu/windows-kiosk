# Kill Explorer

## Setup

1. Copy the file start.bat into your project folder.
1. Change the program path to your application.

## Run

When started, you can close every application by presing **[Alt + F4]**. Execute the start.bat file. **Be aware, that the explorer process will be terminated!**  After closing the explorer will be started again.