# Windows-Kiosk

A project to bundle resources to use a windows device in kiosk modus.

| Methods | Windows 7 | Windows 8 | Windows 10 |
|-|-|-|-|
| kill-explorer | ? | ✔ | ? | 
| toggle-edge-swipes | ? | ? |  ✔ |


## Kill Explorer
Appropriate for: Windows 8

**Attention: Only use this method if you know, what you are doing!** 

Is a script, that kills the *explorer.exe*, then runs the desired app. And when that app is closed, it starts the *explorer.exe* again.

Windows 8 - as great as we all remember it - has no way to turn off all the gestures that are allowed on the platform. This way is the most reliable, but it's quite drastic by killing the *explorer.exe* process. If you run an kiosk app on a local machine, that should be no big deal, also as the explorer get's restarted when the app closes. 


## Toggle Edge Swipes

Appropriate for:
**Windows 10**

Script to disable the default windows edge swipes.
Must be run as administrator!!!

+ Swipe-From-Right: Shows *Action-Center*
+ Swipe-From-Left: Opens overview of running applications.

```powershell
ToggleEdgeSwipes.ps1 enable=0
```

#### Arguments

**-enable**

Reenables the swipes.
