﻿param(
    # Set flag to reenable the interation.
    [switch]$enable=$false
    )

$regedit_edgeUI = ".\SOFTWARE\Policies\Microsoft\Windows"
$key_name = "EdgeUI"
$edgeSwipe = "AllowEdgeSwipe"
$value = If ($enable) {1} Else {0}

$completePath = "$regedit_edgeUI\$key_name"

# Save original location of the power-shell.
Push-Location

#Switch directory to HKEY_LOCAL_MACHINE
Set-Location HKLM:

# Test if the key already exists.
if(-Not(Test-Path $completePath)){
    #If not Create it!
    Write-Output "Created Regestry Key @ $regedit_edgeUI"
    New-Item -Path $regedit_edgeUI -Name $key_name
}

# Test if the variable already exists.
if(Get-ItemProperty -Path $completePath -Name $edgeSwipe){
    # Update it, when it exist.
    Write-Output "Updated $key_name to $value."
    Set-ItemProperty -Path $completePath -Name $edgeSwipe -Value $value
}else{
    # Create it, when it doesn't exist.
    Write-Output "Create $key_name to $value."
    New-ItemProperty -Path $completePath -Name $edgeSwipe -Value $value -PropertyType DWORD
}

#Return to the original location in the power-shell.
Pop-Location


